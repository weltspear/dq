package net.dq.interpreter;

public class dqVariable {

    private final dqType type;
    private dqObject object;

    public dqVariable(dqType type, dqObject object){
        this.type = type;
        this.object = object;
    }

    public dqType getType() {
        return type;
    }

    public dqObject getObject() {
        return object;
    }

    public void setObject(dqObject object) {
        this.object = object;
    }
}
