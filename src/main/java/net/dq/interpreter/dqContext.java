package net.dq.interpreter;

import java.util.HashMap;

public class dqContext {

    private final dqContext parent;
    private final String name;

    private final HashMap<String, dqVariable> context = new HashMap<>();

    public dqContext(dqContext parent, String name){
        this.parent = parent;
        this.name = name;
    }

    public dqContext getParent() {
        return parent;
    }

    public String getName() {
        return name;
    }

    public dqVariable getVar(String name){
        if (!context.containsKey(name)){
            return parent.getVar(name);
        }

        return context.get(name);
    }

    public void setVar(String name, dqVariable variable){
        context.put(name, variable);
    }

    public String trace(){
        String tr = "";
        if (parent != null){
            tr += parent.trace();
        }
        tr += ":" + name;
        return tr;
    }
}
