package net.dq.parser;

import net.dq.lexer.Token;

import java.util.ArrayList;
import java.util.Objects;


public class Parser {

    public abstract static class Node{

    }

    public static class FuncDefNode extends Node{
        private final TypeNode returnType;
        private final ArrayList<TypeIdentifierNode> args;
        private final String name;
        private final ArrayList<Node> functionBody;

        public FuncDefNode(TypeNode return_type, ArrayList<TypeIdentifierNode> args, String name, ArrayList<Node> function_body){
            returnType = return_type;
            this.args = args;
            this.name = name;
            functionBody = function_body;
        }

        public TypeNode getReturnType() {
            return returnType;
        }

        public ArrayList<TypeIdentifierNode> getArgs() {
            return args;
        }

        public String getName() {
            return name;
        }

        public ArrayList<Node> getFunctionBody() {
            return functionBody;
        }
    }

    public static class TypeNode extends Node{
        private final String type;
        private final boolean canBeError;

        public TypeNode(String type, boolean can_be_error){
            this.type = type;
            canBeError = can_be_error;
        }

        public String getType() {
            return type;
        }

        public boolean isCanBeError() {
            return canBeError;
        }
    }

    public static class TypeIdentifierNode extends Node{
        TypeNode typeNode;
        private final String identifier;

        public TypeIdentifierNode(TypeNode typeNode, String identifier){
            this.typeNode = typeNode;
            this.identifier = identifier;
        }

        public TypeNode getTypeNode() {
            return typeNode;
        }

        public String getIdentifier() {
            return identifier;
        }
    }

    public static class TypeCastedExpressionNode extends ExpressionNode{
        private final TypeNode typeNode;

        private final ExpressionNode expressionNode;

        public TypeCastedExpressionNode(ExpressionNode expressionNode, TypeNode typeNode) {
            this.expressionNode = expressionNode;
            this.typeNode = typeNode;
        }

        public TypeNode getTypeNode() {
            return typeNode;
        }

        public ExpressionNode getExpressionNode() {
            return expressionNode;
        }
    }

    private int i = 0;

    public static class VariableDeclarationNode extends Node{
        private final String varname;
        private final ExpressionNode node;
        private final TypeNode t;

        public VariableDeclarationNode(String varname, ExpressionNode node, TypeNode t){
            this.varname = varname;
            this.node = node;
            this.t = t;
        }

        public String getVarname() {
            return varname;
        }

        public ExpressionNode getNode() {
            return node;
        }

        public TypeNode getT() {
            return t;
        }
    }

    public static class VariableWriteNode extends Node{
        private final String varname;
        private final ExpressionNode expr;

        public VariableWriteNode(String varname, ExpressionNode expr){
            this.varname = varname;
            this.expr = expr;
        }

        public String getVarname() {
            return varname;
        }

        public ExpressionNode getExpr() {
            return expr;
        }
    }

    public static class IncreaseDeclaration extends Node{
        private final String varname;

        public IncreaseDeclaration(String varname){
            this.varname = varname;
        }

        public String getVarname() {
            return varname;
        }
    }

    public static class DecreaseDeclaration extends Node{
        private final String varname;

        public DecreaseDeclaration(String varname){
            this.varname = varname;
        }

        public String getVarname() {
            return varname;
        }
    }

    public static class ConditionalNode extends Node{
        private final ArrayList<ExpressionNode> conditions;
        private final ArrayList<ArrayList<Node>> body;

        private final ArrayList<Node> else_body;

        public ConditionalNode(ArrayList<ExpressionNode> conditions, ArrayList<ArrayList<Node>> body, ArrayList<Node> elseBody){
            this.conditions = conditions;

            this.body = body;
            else_body = elseBody;
        }

        public ArrayList<ExpressionNode> getConditions() {
            return conditions;
        }

        public ArrayList<ArrayList<Node>> getBody() {
            return body;
        }

        public ArrayList<Node> getElseBody() {
            return else_body;
        }
    }

    public enum Operator{
        PLUS,
        MINUS,

        MULTIPLY,

        DIVIDE,

        // bool ops
        NEQUALS,
        EQUALS,

        ARBRACKET, // >
        ALBRACKET, // <

        ARBRACKET_EQ, // >=
        ALBRACKET_EQ, // <=

        AND,
        OR,
    }

    public boolean isBoolOp(Operator op) {
        return op == Operator.EQUALS
                || op == Operator.NEQUALS
                || op == Operator.ALBRACKET
                || op == Operator.ARBRACKET
                || op == Operator.ARBRACKET_EQ
                || op == Operator.ALBRACKET_EQ
                || op == Operator.AND
                || op == Operator.OR;
    }

    public abstract static class ExpressionNode extends Node{

    }

    public abstract static class BooleanExpressionNode extends ExpressionNode{

    }

    public static class BooleanOpNode extends BooleanExpressionNode{
        private final ExpressionNode l;
        private final Operator operator;
        private final ExpressionNode r;

        public BooleanOpNode(ExpressionNode l, Operator operator, ExpressionNode r){
            this.l = l;
            this.operator = operator;
            this.r = r;
        }

        public ExpressionNode getL() {
            return l;
        }

        public Operator getOperator() {
            return operator;
        }

        public ExpressionNode getR() {
            return r;
        }
    }

    public static class NumberNode extends ExpressionNode {
        private final Token number;

        public NumberNode(Token number){
            this.number = number;
        }

        public Token getNumber() {
            return number;
        }
    }

    public static class ReturnNode extends Node{
        private final ExpressionNode expr;

        public ReturnNode(ExpressionNode expr){
            this.expr = expr;
        }

        public ExpressionNode getExpr() {
            return expr;
        }
    }

    public static class IdentifierNode extends ExpressionNode{
        private final Token identifier;

        public IdentifierNode(Token identifier){
            this.identifier = identifier;
        }

        public Token getIdentifier() {
            return identifier;
        }
    }

    public static class BinaryOpNode extends ExpressionNode{
        private final Operator op;
        private final ExpressionNode a;
        private final ExpressionNode b;

        public BinaryOpNode(Operator op, ExpressionNode a, ExpressionNode b){
            this.op = op;
            this.a = a;
            this.b = b;
        }

        public Operator getOp() {
            return op;
        }

        public ExpressionNode getA() {
            return a;
        }

        public ExpressionNode getB() {
            return b;
        }
    }

    public static class FuncCallNode extends ExpressionNode{
        private final String funcName;
        private final ArrayList<ExpressionNode> args;

        public FuncCallNode(String func_name, ArrayList<ExpressionNode> args){
            funcName = func_name;
            this.args = args;
        }

        public String getFuncName() {
            return funcName;
        }

        public ArrayList<ExpressionNode> getArgs() {
            return args;
        }
    }

    public ArrayList<Node> parse(ArrayList<Token> tokens){
        ArrayList<Node> nodes = new ArrayList<>();
        while (i < tokens.size()) {
            if (tokens.get(i).getType().equals(Token.Type.KEYWORD)){
                if (Objects.equals(tokens.get(i).getValue(), "fn")){
                    nodes.add(parseFunctionDefinition(tokens));
                }
            }
        }
        return nodes;
    }

    public VariableDeclarationNode parseVariableDeclaration(ArrayList<Token> tokens){
        i++;
        if (tokens.get(i).getType() != Token.Type.IDENTIFIER){
            throw new RuntimeException("Incorrect Token" + " at line " + tokens.get(i).getLine());
        }
        String name = (String) tokens.get(i).getValue();
        i++;
        if (tokens.get(i).getType() != Token.Type.DOUBLE_DOT){
            throw new RuntimeException("Incorrect Token" + " at line " + tokens.get(i).getLine());
        }
        i++;
        TypeNode type = parseTypeNode(tokens);
        if (tokens.get(i).getType() == Token.Type.EQUALS){
            i++;
            ExpressionNode node;
            int old_i = i;

            node = parseExpression(tokens);

            if (tokens.get(i).getType() != Token.Type.COMMA_DOT){
                throw new RuntimeException("Incorrect Token" + " at line " + tokens.get(i).getLine());
            }
            i++;
            return new VariableDeclarationNode(name, node, type);
        }
        else if (tokens.get(i).getType() != Token.Type.COMMA_DOT){
            throw new RuntimeException("Incorrect Token" + " at line " + tokens.get(i).getLine());
        }
        i++;
        return new VariableDeclarationNode(name, null, type);
    }

    private Token safeGet(ArrayList<Token> tokens, int i){
        if (i >= tokens.size()){
            return null;
        }
        else{
            return tokens.get(i);
        }
    }

    private Token.Type safeGetType(ArrayList<Token> tokens, int i){
        if (safeGet(tokens, i) == null){
            return null;
        }
        else{
            return safeGet(tokens, i).getType();
        }
    }

    private ExpressionNode parseBinOp(ArrayList<Token> tokens){
        ExpressionNode l = null;
        Operator op = null;
        ExpressionNode r = null;

        while (i < tokens.size()){
            if (tokens.get(i).getType() == Token.Type.INTEGER ||
                    tokens.get(i).getType() == Token.Type.FLOAT ||
                    tokens.get(i).getType() == Token.Type.DOUBLE ||
                    tokens.get(i).getType() == Token.Type.IDENTIFIER ||
                    tokens.get(i).getType() == Token.Type.LPAREN){
                if (l == null){
                    if (tokens.get(i).getType() == Token.Type.LPAREN){
                        i++;
                        l = parseExpression(tokens);
                        i++;
                        ExpressionNode nd = checkForTypeCastNode(tokens, l);
                        if (nd != null){
                            l = nd;
                        }
                    }
                    else if (tokens.get(i).getType() == Token.Type.INTEGER ||
                            tokens.get(i).getType() == Token.Type.FLOAT ||
                            tokens.get(i).getType() == Token.Type.DOUBLE) {
                        l = new NumberNode(tokens.get(i));
                        i++;
                        ExpressionNode nd = checkForTypeCastNode(tokens, l);
                        if (nd != null){
                            l = nd;
                        }
                    }
                    else {
                        if (safeGetType(tokens, i+1) == Token.Type.LPAREN){
                            l = parseFuncCall(tokens);
                            ExpressionNode nd = checkForTypeCastNode(tokens, l);
                            if (nd != null){
                                l = nd;
                            }
                        }
                        else{
                            l = new IdentifierNode(tokens.get(i));
                            i++;
                            ExpressionNode nd = checkForTypeCastNode(tokens, l);
                            if (nd != null){
                                l = nd;
                            }
                        }
                    }
                }
                else{
                    if (tokens.get(i).getType() == Token.Type.LPAREN){
                        i++;
                        r = parseExpression(tokens);
                        i++;
                        ExpressionNode nd = checkForTypeCastNode(tokens, r);
                        if (nd != null){
                            r = nd;
                        }
                    }
                    else if (tokens.get(i).getType() == Token.Type.INTEGER ||
                            tokens.get(i).getType() == Token.Type.FLOAT ||
                            tokens.get(i).getType() == Token.Type.DOUBLE) {
                        r = new NumberNode(tokens.get(i));
                        i++;
                    }

                    else{
                        if (safeGetType(tokens, i+1) == Token.Type.LPAREN){
                            r = parseFuncCall(tokens);
                        }
                        else {
                            r = new IdentifierNode(tokens.get(i));
                            i++;
                        }
                    }
                }
            }

            if (l != null && op != null && r != null){
                ExpressionNode nd = isBoolOp(op) ? new BooleanOpNode(l, op, r) : new BinaryOpNode(op, l, r);
                l = nd;
                op = null;
                r = null;
            }

            if (tokens.get(i).getType() == Token.Type.COMMA_DOT
                    || tokens.get(i).getType() == Token.Type.RPAREN
                    || tokens.get(i).getType() == Token.Type.PLUS
                    || tokens.get(i).getType() == Token.Type.MINUS
                    || tokens.get(i).getType() == Token.Type.COMMA
                    || tokens.get(i).getType() == Token.Type.AND || tokens.get(i).getType() == Token.Type.OR

            ){
                break;
            }

            else if (tokens.get(i).getType() == Token.Type.MULTIPLY || tokens.get(i).getType() == Token.Type.DIVIDE ||
                    tokens.get(i).getType() == Token.Type.EQEQ
                    || tokens.get(i).getType() == Token.Type.NEQUALS
                    || tokens.get(i).getType() == Token.Type.ALBRACKET
                    || tokens.get(i).getType() == Token.Type.ARBRACKET
                    || tokens.get(i).getType() == Token.Type.ARBRACKET_EQ
                    || tokens.get(i).getType() == Token.Type.ALBRACKET_EQ){
                if (op != null){
                    throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                }
                op = tokens.get(i).getType() == Token.Type.MULTIPLY ? Operator.MULTIPLY: tokens.get(i).getType() == Token.Type.EQEQ?Operator.EQUALS :
                        tokens.get(i).getType() == Token.Type.NEQUALS?Operator.NEQUALS :
                                tokens.get(i).getType() == Token.Type.ALBRACKET?Operator.ALBRACKET :
                                        tokens.get(i).getType() == Token.Type.ARBRACKET?Operator.ARBRACKET :
                                                tokens.get(i).getType() == Token.Type.ARBRACKET_EQ?Operator.ARBRACKET_EQ:
                                                        tokens.get(i).getType() == Token.Type.ALBRACKET_EQ?Operator.ALBRACKET_EQ
                :Operator.DIVIDE;
                i++;
            }

            else{
                throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
            }

        }

        if (op == null){
            return l;
        }

        return new BinaryOpNode(op, l, r);
    }

    private ExpressionNode checkForTypeCastNode(ArrayList<Token> tokens, ExpressionNode expr){
        if (safeGetType(tokens, i) == Token.Type.KEYWORD){
            Token tok = safeGet(tokens, i);
            String keyword = (String)tok.getValue();
            if (keyword.equals("as")){
                i++;
                TypeNode nd = parseTypeNode(tokens);
                return new TypeCastedExpressionNode(expr, nd);
            }
            else return null;
        }
        return null;
    }

    private ExpressionNode parseExpression(ArrayList<Token> tokens) {
        // binopnode
        ExpressionNode l = null;
        Operator op = null;
        ExpressionNode r = null;

        while (i < tokens.size()) {

            if (l == null) {
                // very hacky
                if (tokens.get(i).getType() == Token.Type.PLUS) {
                    if (this.safeGetType(tokens, i + 1) == Token.Type.PLUS) {
                        throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                    }
                    i++;
                    continue;
                } else if (tokens.get(i).getType() == Token.Type.MINUS) {
                    l = new NumberNode(new Token(Token.Type.INTEGER, 0));
                    op = Operator.MINUS;
                    i++;
                    continue;
                }

                if (tokens.get(i).getType() == Token.Type.LPAREN) {
                    i++;
                    l = parseExpression(tokens);
                    i++;
                    ExpressionNode nd = checkForTypeCastNode(tokens, l);
                    if (nd != null) {
                        l = nd;
                    }
                } else
                    l = parseBinOp(tokens);
            } else {
                if (tokens.get(i).getType() == Token.Type.LPAREN) {
                    i++;
                    r = parseExpression(tokens);
                    i++;
                    ExpressionNode nd = checkForTypeCastNode(tokens, r);
                    if (nd != null) {
                        r = nd;
                    }
                } else
                    r = parseBinOp(tokens);
            }

            if (l != null && op != null && r != null) {
                ExpressionNode nd = isBoolOp(op) ? new BooleanOpNode(l, op, r) : new BinaryOpNode(op, l, r);
                l = nd;
                op = null;
                r = null;
            }

            if (tokens.get(i).getType() == Token.Type.COMMA_DOT || tokens.get(i).getType() == Token.Type.RPAREN
                    || tokens.get(i).getType() == Token.Type.COMMA ||
                    tokens.get(i).getType() == Token.Type.EQEQ
                    || tokens.get(i).getType() == Token.Type.NEQUALS
                    || tokens.get(i).getType() == Token.Type.ALBRACKET
                    || tokens.get(i).getType() == Token.Type.ARBRACKET
                    || tokens.get(i).getType() == Token.Type.ARBRACKET_EQ
                    || tokens.get(i).getType() == Token.Type.ALBRACKET_EQ) {
                break;
            } else if (tokens.get(i).getType() == Token.Type.PLUS || tokens.get(i).getType() == Token.Type.MINUS
                    || tokens.get(i).getType() == Token.Type.AND || tokens.get(i).getType() == Token.Type.OR) {
                if (op != null) {
                    throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                }
                //|| tokens.get(i).getType() == Token.Type.AND || tokens.get(i).getType() == Token.Type.OR
                op = tokens.get(i).getType() == Token.Type.PLUS ? Operator.PLUS :
                        tokens.get(i).getType() == Token.Type.MINUS ? Operator.MINUS :
                                tokens.get(i).getType() == Token.Type.AND ? Operator.AND: Operator.OR;
                i++;
            } else {
                throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
            }

        }

        if (op == null)
            return l;

        return new BinaryOpNode(op, l, r);
    }

    public FuncDefNode parseFunctionDefinition(ArrayList<Token> tokens){
        i++;
        TypeNode typeNode = parseTypeNode(tokens);
        String fname;
        if (tokens.get(i).getType() == Token.Type.IDENTIFIER ){
            fname = (String) tokens.get(i).getValue();
        }
        else{
            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
        }
        i++;
        if (tokens.get(i).getType() != Token.Type.LPAREN){
            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
        }

        ArrayList<TypeIdentifierNode> args = new ArrayList<>();
        i++;

        while (i<tokens.size()) {
            if (tokens.get(i).getType() == Token.Type.RPAREN){
                i++;
                break;
            }
            TypeNode t = parseTypeNode(tokens);
            if (tokens.get(i).getType() != Token.Type.IDENTIFIER){
                throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
            }
            args.add(new TypeIdentifierNode(t, (String) tokens.get(i).getValue()));
            i++;
            if (tokens.get(i).getType() == Token.Type.RPAREN){
                i++;
                break;
            }
            if (tokens.get(i).getType() == Token.Type.COMMA){
                i++;
            }
            else{
                throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
            }
            System.out.println();
        }

        if (tokens.get(i).getType() == Token.Type.COMMA_DOT){
            return new FuncDefNode(typeNode, args, fname, null);
        } else if (tokens.get(i).getType() == Token.Type.LBRACE) {

            return new FuncDefNode(typeNode, args, fname, parseBlockBody(tokens));
        }
        throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
    }

    public ArrayList<Node> parseBlockBody(ArrayList<Token> tokens){
        i++;
        ArrayList<Node> body = new ArrayList<>();
        // parse func body
        while (i<tokens.size()){
            if (tokens.get(i).getType() == Token.Type.KEYWORD){
                String keyword = (String) tokens.get(i).getValue();
                switch (keyword) {
                    case "var" -> body.add(parseVariableDeclaration(tokens));
                    case "return" -> body.add(parseReturnStatement(tokens));
                    case "if" -> body.add(parseConditionalStatement(tokens));
                    default -> throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                }
            }

            if (tokens.get(i).getType() == Token.Type.IDENTIFIER){
                Token next = safeGet(tokens, i+1);
                Token next2 = safeGet(tokens, i+2);

                if (next != null){
                    if (next.getType() == Token.Type.EQUALS){
                        body.add(parseVariableWrite(tokens));
                    }
                    else if (next.getType() == Token.Type.LPAREN){
                        body.add(parseFuncCall(tokens));
                        if (tokens.get(i).getType() != Token.Type.COMMA_DOT){
                            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                        }
                        i++;
                    }
                    if (next.getType() == Token.Type.PLUS) {
                        if (next2 != null)
                            if (next2.getType() == Token.Type.PLUS)
                                body.add(parseIncreaseDeclaration(tokens));
                            else throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                        else throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                    }
                    else if (next.getType() == Token.Type.MINUS){
                        if (next2 != null)
                            if (next2.getType() == Token.Type.MINUS)
                                body.add(parseDecreaseDeclaration(tokens));
                            else throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                        else throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                    }
                }
                else{
                    throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                }
            }

            if (tokens.get(i).getType() == Token.Type.RBRACE){
                i++;
                break;
            }
        }
        return body;
    }

    public ConditionalNode parseConditionalStatement(ArrayList<Token> tokens){
        ArrayList<ExpressionNode> conditions = new ArrayList<>();
        ArrayList<ArrayList<Node>> body = new ArrayList<>();
        ArrayList<Node> else_body = null;
        i++;
        if (safeGetType(tokens, i) == Token.Type.LPAREN){
            i++;
            conditions.add(parseExpression(tokens));
            if (safeGetType(tokens, i) == Token.Type.RPAREN){
                i++;
                if (safeGetType(tokens, i) == Token.Type.LBRACE){
                    body.add(parseBlockBody(tokens));
                }
                else {
                    throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                }
            }
            else {
                throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
            }
        }
        else {
            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
        }

        while (true){
            if (safeGetType(tokens, i) == Token.Type.KEYWORD){
                if (safeGet(tokens, i).getValue().equals("else")){
                    i++;
                    if (safeGetType(tokens, i) == Token.Type.KEYWORD){
                        if (safeGet(tokens, i).getValue().equals("if")){
                            i++;
                            if (safeGetType(tokens, i) == Token.Type.LPAREN){
                                i++;
                                conditions.add(parseExpression(tokens));
                                if (safeGetType(tokens, i) == Token.Type.RPAREN){
                                    i++;
                                    if (safeGetType(tokens, i) == Token.Type.LBRACE){
                                        body.add(parseBlockBody(tokens));
                                    }
                                    else {
                                        throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                                    }
                                }
                                else {
                                    throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                                }
                            }
                            else {
                                throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                            }
                        }
                        else {
                            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                        }

                    }
                    else{
                        if (safeGetType(tokens, i) == Token.Type.LBRACE){
                            else_body = (parseBlockBody(tokens));
                            break;
                        }
                        else {
                            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
                        }
                    }
                }
            }
            else{
                break;
            }
        }

        return new ConditionalNode(conditions, body, else_body);
    }

    private ReturnNode parseReturnStatement(ArrayList<Token> tokens) {
        i++;
        ExpressionNode expressionNode = parseExpression(tokens);

        if (tokens.get(i).getType() != Token.Type.COMMA_DOT){
            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
        }
        i++;

        return new ReturnNode(expressionNode);
    }

    private VariableWriteNode parseVariableWrite(ArrayList<Token> tokens){
        if (tokens.get(i).getType() != Token.Type.IDENTIFIER){
            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
        }
        String varname = (String) tokens.get(i).getValue();
        i++;

        if (tokens.get(i).getType() != Token.Type.EQUALS){
            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
        }
        i++;

        ExpressionNode expr = parseExpression(tokens);

        if (tokens.get(i).getType() != Token.Type.COMMA_DOT){
            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
        }
        i++;

        return new VariableWriteNode(varname, expr);
    }

    public TypeNode parseTypeNode(ArrayList<Token> tokens){
        if (tokens.get(i).getType() == Token.Type.IDENTIFIER || tokens.get(i).getType() == Token.Type.KEYWORD) {
            /*
            int
            float
            long
            double
             */

            String type = (String) tokens.get(i).getValue();
            if (tokens.get(i + 1).getType() == Token.Type.EXCLAMATION_MARK) {
                i+=2;
                return new TypeNode(type, true);
            }
            i+=1;
            return new TypeNode(type, false);
        }
        else{
            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
        }
    }

    public FuncCallNode parseFuncCall(ArrayList<Token> tokens){
        String fname = (String) tokens.get(i).getValue();
        ArrayList<ExpressionNode> args = new ArrayList<>();
        i++;

        if (tokens.get(i).getType() != Token.Type.LPAREN){
            throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
        }
        i++;

        while (true){
            if (tokens.get(i).getType() == Token.Type.RPAREN){
                i++;
                break;
            }
            args.add(parseExpression(tokens));
            if (tokens.get(i).getType() == Token.Type.COMMA){
                i++;
            }
            else if (tokens.get(i).getType() == Token.Type.RPAREN){
                i++;
                break;
            }
            else{
                throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
            }
        }

        return new FuncCallNode(fname, args);
    }

    public IncreaseDeclaration parseIncreaseDeclaration(ArrayList<Token> tokens){
        try{
            return new IncreaseDeclaration((String) safeGet(tokens, i).getValue());
        }
        finally {
            i+=3;
            if (safeGetType(tokens, i) != Token.Type.COMMA_DOT){
                throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
            }
            i+=1;
        }
    }

    public DecreaseDeclaration parseDecreaseDeclaration(ArrayList<Token> tokens){
        try{
            return new DecreaseDeclaration((String) safeGet(tokens, i).getValue());
        }
        finally {
            i+=3;
            if (safeGetType(tokens, i) != Token.Type.COMMA_DOT){
                throw new RuntimeException("Incorrect token at line " + tokens.get(i).getLine());
            }
            i+=1;
        }
    }

}
