package net.dq.interpreter;

public class dqType {
    private final PrimitiveTypes type;

    public enum PrimitiveTypes{
        FUNC,
        INT,
        DOUBLE,
        FLOAT,
        LONG,
        DQType,
        BOOL
    }

    public dqType(PrimitiveTypes type){

        this.type = type;
    }

    public PrimitiveTypes getType() {
        return type;
    }
}
