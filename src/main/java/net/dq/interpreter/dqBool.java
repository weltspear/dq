package net.dq.interpreter;

public class dqBool extends dqObject{

    private final boolean bool;

    public dqBool(boolean bool){
        this.bool = bool;
    }

    @Override
    public dqType getType() {
        return Types.d_BOOL;
    }

    public boolean getBool() {
        return bool;
    }
}
