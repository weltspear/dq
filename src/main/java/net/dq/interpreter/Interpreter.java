package net.dq.interpreter;

import net.dq.lexer.Token;
import net.dq.parser.Parser;

import java.util.ArrayList;

import static net.dq.interpreter.Types.*;

public class Interpreter {

    private final dqContext master = new dqContext(null, "<master>");

    public Interpreter(ArrayList<Parser.Node> nodes){
        for (Parser.Node node: nodes){
            if (node instanceof Parser.FuncDefNode funcDefNode){
                master.setVar(funcDefNode.getName(), new dqVariable(
                        new dqType(dqType.PrimitiveTypes.FUNC),
                        new dqFunction(funcDefNode.getFunctionBody(), funcDefNode.getArgs(), funcDefNode.getReturnType())));
            }
        }
    }

    public void run(){
        callFunction("main", master);
    }

    public dqObject callFunction(String func_name, dqContext _ctx, dqObject... args){
        dqVariable variable = _ctx.getVar(func_name);

        if (variable.getType().getType() == dqType.PrimitiveTypes.FUNC){
            dqContext context = new dqContext(_ctx, func_name);
            dqFunction func = (dqFunction) variable.getObject();

            // args setup
            int i = 0;
            for (dqObject object: args){
                // primitive numerical
                switch (func.getArgs().get(i).getTypeNode().getType()) {
                    case "int" -> context.setVar(func.getArgs().get(i).getIdentifier(), new dqVariable(
                            d_INT, object
                    ));
                    case "double" -> context.setVar(func.getArgs().get(i).getIdentifier(), new dqVariable(
                            d_DOUBLE, object
                    ));
                    case "float" -> context.setVar(func.getArgs().get(i).getIdentifier(), new dqVariable(
                            d_FLOAT, object
                    ));
                }

                i++;
            }

            return visitBlock(func.getFunctionBody(), context, func);
        }
        else{
            throw new RuntimeException("No function " + func_name + " defined" + " Trace: " + _ctx.trace());
        }
    }

    public dqObject callFunction(String func_name, dqObject... args){
        return callFunction(func_name, master, args);
    }

    public dqObject visitReturnNode(Parser.ReturnNode returnNode, dqFunction function, dqContext context){
        dqObject out = visitExpression(returnNode.getExpr(), context);
        if (out.getType() != str2dqType(function.getReturnType().getType())){
            throw new RuntimeException("Incorrect type. Trace: " + context.trace());
        }
        return out;
    }

    public dqObject visitConditionalNode(Parser.ConditionalNode conditionalNode, dqContext context, dqFunction func){
        boolean ifexecuted = false;
        for (int i = 0; i < conditionalNode.getConditions().size(); i++){
            Parser.ExpressionNode condition = conditionalNode.getConditions().get(i);
            if (condition instanceof Parser.BooleanOpNode booleanOpNode){
                dqBool conditionOut = (dqBool) visitExpression(condition, context);
                if (conditionOut.getBool()){
                    dqObject out = visitBlock(conditionalNode.getBody().get(i), context, func);
                    if (out != null){
                        return out;
                    }
                    ifexecuted = true;
                    break;
                }
            }
            else{
                throw new RuntimeException("Incorrect type. Trace: " + context.trace());
            }
        }

        if (!ifexecuted){
            return visitBlock(conditionalNode.getElseBody(), context, func);
        }

        return null;
    }

    public dqObject visitBlock(ArrayList<Parser.Node> block, dqContext context, dqFunction func){
        for (Parser.Node node: block){
            if (node instanceof Parser.VariableDeclarationNode variableDeclarationNode){
                visitVarDeclarationNode(variableDeclarationNode, context);
            }
            if (node instanceof Parser.ReturnNode returnNode){
                //return visitExpression(returnNode.getExpr(), context);
                return visitReturnNode(returnNode, func ,context);
            }
            if (node instanceof Parser.VariableWriteNode variableWriteNode){
                visitVarWriteNode(variableWriteNode, context);
            }
            if (node instanceof Parser.FuncCallNode funcCallNode){
                visitFuncCall(funcCallNode, context);
            }
            if (node instanceof Parser.IncreaseDeclaration increaseDeclaration){
                visitIncreaseDeclaration(increaseDeclaration, context);
            }
            if (node instanceof Parser.DecreaseDeclaration decreaseDeclaration){
                visitDecreaseDeclaration(decreaseDeclaration, context);
            }
            if (node instanceof Parser.ConditionalNode _conditionalNode){
                dqObject out = visitConditionalNode(_conditionalNode, context, func);
                if (out != null){
                    return out;
                }
            }

            else{
                throw new RuntimeException("Illegal statement. Trace: " + context.trace());
            }
        }
        return null;
    }

    public dqType str2dqType(String type){
        switch (type) {
            case "int" -> {
                return d_INT;
            }
            case "double" -> {
                return d_DOUBLE;
            }
            case "long" -> {
                return d_LONG;
            }
            case "float" -> {
                return d_FLOAT;
            }
            case "bool" -> {
                return d_BOOL;
            }
        }
        return null;
    }

    public void visitVarDeclarationNode(Parser.VariableDeclarationNode variableDeclarationNode, dqContext ctx){
        dqObject out = visitExpression(variableDeclarationNode.getNode(), ctx);
        switch (variableDeclarationNode.getT().getType()) {
            case "int" -> {
                if (out instanceof dqNumericBox nn) {
                    if (nn.getType().getType() != dqType.PrimitiveTypes.INT)
                        throw new RuntimeException("Incorrect type at declaration of '" + variableDeclarationNode.getVarname() + "'" + " Trace: " + ctx.trace());
                    ctx.setVar(variableDeclarationNode.getVarname(), new dqVariable(
                            d_INT, out));
                }
                else{
                    throw new RuntimeException("Incorrect type at declaration of '" + variableDeclarationNode.getVarname() + "'" + " Trace: " + ctx.trace());
                }
            }
            case "double" -> {
                if (out instanceof dqNumericBox nn) {
                    if (nn.getType().getType() != dqType.PrimitiveTypes.DOUBLE)
                        throw new RuntimeException("Incorrect type at declaration of '" + variableDeclarationNode.getVarname() + "'" + " Trace: " + ctx.trace());
                    ctx.setVar(variableDeclarationNode.getVarname(), new dqVariable(
                            d_DOUBLE, out));
                }
                else{
                    throw new RuntimeException("Incorrect type at declaration of '" + variableDeclarationNode.getVarname() + "'" + " Trace: " + ctx.trace());
                }
            }
            case "float" -> {
                if (out instanceof dqNumericBox nn) {
                    if (nn.getType().getType() != dqType.PrimitiveTypes.FLOAT)
                        throw new RuntimeException("Incorrect type at declaration of '" + variableDeclarationNode.getVarname() + "'" + " Trace: " + ctx.trace());
                    ctx.setVar(variableDeclarationNode.getVarname(), new dqVariable(
                            d_FLOAT, out));
                }
                else{
                    throw new RuntimeException("Incorrect type at declaration of '" + variableDeclarationNode.getVarname() + "'" + " Trace: " + ctx.trace());
                }
            }
            case "long" -> {
                if (out instanceof dqNumericBox nn) {
                    if (nn.getType().getType() != dqType.PrimitiveTypes.LONG)
                        throw new RuntimeException("Incorrect type at declaration of '" + variableDeclarationNode.getVarname() + "'" + " Trace: " + ctx.trace());
                    ctx.setVar(variableDeclarationNode.getVarname(), new dqVariable(
                            d_LONG, out));
                }
                else{
                    throw new RuntimeException("Incorrect type at declaration of '" + variableDeclarationNode.getVarname() + "'" + " Trace: " + ctx.trace());
                }
            }
            case "bool" -> {
                if (out instanceof dqBool) {
                    ctx.setVar(variableDeclarationNode.getVarname(), new dqVariable(
                            d_BOOL, out));
                }
                else {
                    throw new RuntimeException("Incorrect type at declaration of '" + variableDeclarationNode.getVarname() + "'" + " Trace: " + ctx.trace());
                }
            }
        }
    }
    
    public void visitVarWriteNode(Parser.VariableWriteNode variableWriteNode, dqContext ctx){
        dqObject out = visitExpression(variableWriteNode.getExpr(), ctx);
        
        if (ctx.getVar(variableWriteNode.getVarname()).getType() != out.getType()){
            throw new RuntimeException("Incorrect type. Trace: " + ctx.trace());
        }
        ctx.getVar(variableWriteNode.getVarname()).setObject(out);
    }

    public dqObject visitFuncCall(Parser.FuncCallNode funcCallNode, dqContext ctx){
        String func = funcCallNode.getFuncName();
        ArrayList<Parser.ExpressionNode> _args = funcCallNode.getArgs();

        dqObject[] args = new dqObject[_args.size()];

        int i = 0;
        for (Parser.ExpressionNode _nd : _args){
            args[i] = visitExpression(_nd, ctx);
            i++;
        }

        return callFunction(func, ctx, args);
    }

    public dqObject visitExpression(Parser.ExpressionNode node, dqContext ctx){
        if (node instanceof Parser.NumberNode nn){
            return new dqNumericBox((Number) nn.getNumber().getValue(), tt2dqType(nn.getNumber().getType()));
        }
        if (node instanceof Parser.IdentifierNode in){
            String i = (String) in.getIdentifier().getValue();
            
            if (i.equals("true")){
                return new dqBool(true);
            }
            else if (i.equals("false")){
                return new dqBool(false);
            }

            return ctx.getVar((String) in.getIdentifier().getValue()).getObject();
        }
        if (node instanceof Parser.FuncCallNode funcCallNode){
            return visitFuncCall(funcCallNode, ctx);
        }
        else if (node instanceof Parser.BinaryOpNode binop){
            dqObject a = visitExpression(binop.getA(), ctx);
            dqObject b = visitExpression(binop.getB(), ctx);

            if (!(a instanceof dqNumericBox a_n) || !(b instanceof dqNumericBox b_n)){
                throw new RuntimeException("Not numeric type. Trace: " + ctx.trace());
            }

            if (binop.getOp() == Parser.Operator.MULTIPLY){
                dqType type = inferNumType(a, b);
                if (type.getType() == dqType.PrimitiveTypes.INT){
                    return new dqNumericBox(a_n.getNumber().intValue()*b_n.getNumber().intValue(), type);
                }
                else if (type.getType() == dqType.PrimitiveTypes.LONG){
                    return new dqNumericBox(a_n.getNumber().longValue()*b_n.getNumber().longValue(), type);
                }
                else if (type.getType() == dqType.PrimitiveTypes.FLOAT){
                    return new dqNumericBox(a_n.getNumber().floatValue()*b_n.getNumber().floatValue(), type);
                }
                else{
                    return new dqNumericBox(a_n.getNumber().doubleValue()*b_n.getNumber().doubleValue(), type);
                }
            } else if (binop.getOp() == Parser.Operator.DIVIDE) {
                dqType type = inferNumType(a, b);
                if (type.getType() == dqType.PrimitiveTypes.INT){
                    return new dqNumericBox(a_n.getNumber().intValue()/b_n.getNumber().intValue(), type);
                }
                else if (type.getType() == dqType.PrimitiveTypes.LONG){
                    return new dqNumericBox(a_n.getNumber().longValue()/b_n.getNumber().longValue(), type);
                }
                else if (type.getType() == dqType.PrimitiveTypes.FLOAT){
                    return new dqNumericBox(a_n.getNumber().floatValue()/b_n.getNumber().floatValue(), type);
                }
                else{
                    return new dqNumericBox(a_n.getNumber().doubleValue()/b_n.getNumber().doubleValue(), type);
                }
            }
            else if (binop.getOp() == Parser.Operator.PLUS) {
                dqType type = inferNumType(a, b);
                if (type.getType() == dqType.PrimitiveTypes.INT){
                    return new dqNumericBox(a_n.getNumber().intValue()+b_n.getNumber().intValue(), type);
                }
                else if (type.getType() == dqType.PrimitiveTypes.LONG){
                    return new dqNumericBox(a_n.getNumber().longValue()+b_n.getNumber().longValue(), type);
                }
                else if (type.getType() == dqType.PrimitiveTypes.FLOAT){
                    return new dqNumericBox(a_n.getNumber().floatValue()+b_n.getNumber().floatValue(), type);
                }
                else{
                    return new dqNumericBox(a_n.getNumber().doubleValue()+b_n.getNumber().doubleValue(), type);
                }
            }
            else if (binop.getOp() == Parser.Operator.MINUS) {
                dqType type = inferNumType(a, b);
                if (type.getType() == dqType.PrimitiveTypes.INT){
                    return new dqNumericBox(a_n.getNumber().intValue()-b_n.getNumber().intValue(), type);
                }
                else if (type.getType() == dqType.PrimitiveTypes.LONG){
                    return new dqNumericBox(a_n.getNumber().longValue()-b_n.getNumber().longValue(), type);
                }
                else if (type.getType() == dqType.PrimitiveTypes.FLOAT){
                    return new dqNumericBox(a_n.getNumber().floatValue()-b_n.getNumber().floatValue(), type);
                }
                else{
                    return new dqNumericBox(a_n.getNumber().doubleValue()-b_n.getNumber().doubleValue(), type);
                }
            }
        }
        else if (node instanceof Parser.TypeCastedExpressionNode typeCast){
            dqObject expr = visitExpression(typeCast.getExpressionNode(), ctx);
            if (expr instanceof dqNumericBox numericBox){
                if (str2dqType(typeCast.getTypeNode().getType()) == d_INT ){
                    return new dqNumericBox(numericBox.getNumber().intValue(),d_INT);
                }
                else if (str2dqType(typeCast.getTypeNode().getType()) == d_FLOAT ){
                    return new dqNumericBox(numericBox.getNumber().floatValue(),d_FLOAT);
                }
                if (str2dqType(typeCast.getTypeNode().getType()) == d_LONG ){
                    return new dqNumericBox(numericBox.getNumber().longValue(),d_LONG);
                }
                if (str2dqType(typeCast.getTypeNode().getType()) == d_DOUBLE ){
                    return new dqNumericBox(numericBox.getNumber().doubleValue(),d_DOUBLE);
                }
                if (str2dqType(typeCast.getTypeNode().getType()) == d_BOOL ){
                    if ((expr.getType() == d_INT || expr.getType() == d_LONG) && expr instanceof dqNumericBox nn)
                        return new dqBool(nn.getNumber().intValue() == 1? true: false);
                }
                else {
                    throw new RuntimeException("Illegal type cast. Trace: " + ctx.trace());
                }
            }
            else if (expr instanceof dqBool bool){
                if (str2dqType(typeCast.getTypeNode().getType()) == d_INT ){
                    return new dqNumericBox((int)(bool.getBool() ? 1 : 0),d_INT);
                }
                if (str2dqType(typeCast.getTypeNode().getType()) == d_LONG ){
                    return new dqNumericBox((long)(bool.getBool() ? 1 : 0),d_LONG);
                }
                else {
                    throw new RuntimeException("Illegal type cast. Trace: " + ctx.trace());
                }
            }
            else {
                throw new RuntimeException("Illegal type cast. Trace: " + ctx.trace());
            }
        } else if (node instanceof Parser.BooleanOpNode){

            dqObject a = visitExpression(((Parser.BooleanOpNode) node).getL(), ctx);
            dqObject b = visitExpression(((Parser.BooleanOpNode) node).getR(), ctx);

            if (((Parser.BooleanOpNode) node).getOperator() == Parser.Operator.EQUALS){
                if (a.getType() != b.getType()){
                    return new dqBool(false);
                }
                else{
                    if (a instanceof dqNumericBox){
                        if (((dqNumericBox) a).getNumber().equals(((dqNumericBox)(b)).getNumber())){
                            return new dqBool(true);
                        }
                        else return new dqBool(false);
                    }
                    if (a instanceof dqBool ab && b instanceof dqBool bb){
                        if (ab.getBool() == bb.getBool()){
                            return new dqBool(true);
                        }
                        else return new dqBool(false);
                    }
                    else {
                        throw new RuntimeException("Illegal expression. Trace: " + ctx.trace());
                    }
                }
            }
            else if (((Parser.BooleanOpNode) node).getOperator() == Parser.Operator.NEQUALS){
                if (a.getType() != b.getType()){
                    return new dqBool(true);
                }
                else{
                    if (a instanceof dqNumericBox){
                        if (((dqNumericBox) a).getNumber().equals(((dqNumericBox)(b)).getNumber())){
                            return new dqBool(false);
                        }
                        else return new dqBool(true);
                    }
                    if (a instanceof dqBool ab && b instanceof dqBool bb){
                        if (ab.getBool() != bb.getBool()){
                            return new dqBool(true);
                        }
                        else return new dqBool(false);
                    }
                    else {
                        throw new RuntimeException("Illegal expression. Trace: " + ctx.trace());
                    }
                }
            }
            else if (((Parser.BooleanOpNode) node).getOperator() == Parser.Operator.ARBRACKET) {
                if (a instanceof dqNumericBox an && b instanceof dqNumericBox bn) {
                    if (a.getType() == d_INT){
                        if (an.getNumber().intValue() > bn.getNumber().intValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_FLOAT){
                        if (an.getNumber().floatValue() > bn.getNumber().floatValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_DOUBLE){
                        if (an.getNumber().doubleValue() > bn.getNumber().doubleValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_LONG){
                        if (an.getNumber().longValue() > bn.getNumber().longValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else {
                        throw new RuntimeException("Illegal expression. Trace: " + ctx.trace());
                    }
                }
            }

            else if (((Parser.BooleanOpNode) node).getOperator() == Parser.Operator.ALBRACKET) {
                if (a instanceof dqNumericBox an && b instanceof dqNumericBox bn) {
                    if (a.getType() == d_INT){
                        if (an.getNumber().intValue() < bn.getNumber().intValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_FLOAT){
                        if (an.getNumber().floatValue() < bn.getNumber().floatValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_DOUBLE){
                        if (an.getNumber().doubleValue() < bn.getNumber().doubleValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_LONG){
                        if (an.getNumber().longValue() < bn.getNumber().longValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else {
                        throw new RuntimeException("Illegal expression. Trace: " + ctx.trace());
                    }
                }
            }

            else if (((Parser.BooleanOpNode) node).getOperator() == Parser.Operator.ARBRACKET_EQ) {
                if (a instanceof dqNumericBox an && b instanceof dqNumericBox bn) {
                    if (a.getType() == d_INT){
                        if (an.getNumber().intValue() >= bn.getNumber().intValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_FLOAT){
                        if (an.getNumber().floatValue() >= bn.getNumber().floatValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_DOUBLE){
                        if (an.getNumber().doubleValue() >= bn.getNumber().doubleValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_LONG){
                        if (an.getNumber().longValue() >= bn.getNumber().longValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else {
                        throw new RuntimeException("Illegal expression. Trace: " + ctx.trace());
                    }
                }
            }

            else if (((Parser.BooleanOpNode) node).getOperator() == Parser.Operator.ALBRACKET_EQ) {
                if (a instanceof dqNumericBox an && b instanceof dqNumericBox bn) {
                    if (a.getType() == d_INT){
                        if (an.getNumber().intValue() <= bn.getNumber().intValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_FLOAT){
                        if (an.getNumber().floatValue() <= bn.getNumber().floatValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_DOUBLE){
                        if (an.getNumber().doubleValue() <= bn.getNumber().doubleValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else if (a.getType() == d_LONG){
                        if (an.getNumber().longValue() <= bn.getNumber().longValue()){
                            return new dqBool(true);
                        }
                        else{
                            return new dqBool(false);
                        }
                    }
                    else {
                        throw new RuntimeException("Illegal expression. Trace: " + ctx.trace());
                    }
                }
            }

            else if (((Parser.BooleanOpNode) node).getOperator() == Parser.Operator.AND){
                if (a instanceof dqBool ab && b instanceof dqBool bb){
                    if (ab.getBool() && bb.getBool()){
                        return new dqBool(true);
                    }
                    else{
                        return new dqBool(false);
                    }
                }
                else {
                    throw new RuntimeException("Illegal expression. Trace: " + ctx.trace());
                }
            }

            else if (((Parser.BooleanOpNode) node).getOperator() == Parser.Operator.OR){
                if (a instanceof dqBool ab && b instanceof dqBool bb){
                    if (ab.getBool() || bb.getBool()){
                        return new dqBool(true);
                    }
                    else{
                        return new dqBool(false);
                    }
                }
                else {
                    throw new RuntimeException("Illegal expression. Trace: " + ctx.trace());
                }
            }
        }

        return null;
    }

    public void visitIncreaseDeclaration(Parser.IncreaseDeclaration increaseDeclaration, dqContext ctx){
        dqVariable var = ctx.getVar(increaseDeclaration.getVarname());
        if (var == null){
            throw new RuntimeException("Value '" + increaseDeclaration.getVarname() + "' not found. Trace: " + ctx.trace());
        }
        else{
            if (var.getType() == d_DOUBLE){
                dqNumericBox nn = (dqNumericBox) var.getObject();
                var.setObject(new dqNumericBox(nn.getNumber().doubleValue() + 1, nn.getType()));
            }
            if (var.getType() == d_FLOAT){
                dqNumericBox nn = (dqNumericBox) var.getObject();
                var.setObject(new dqNumericBox(nn.getNumber().floatValue() + 1, nn.getType()));
            }
            if (var.getType() == d_INT){
                dqNumericBox nn = (dqNumericBox) var.getObject();
                var.setObject(new dqNumericBox(nn.getNumber().intValue() + 1, nn.getType()));
            }
            if (var.getType() == d_LONG){
                dqNumericBox nn = (dqNumericBox) var.getObject();
                var.setObject(new dqNumericBox(nn.getNumber().longValue() + 1, nn.getType()));
            }
        }
    }

    public void visitDecreaseDeclaration(Parser.DecreaseDeclaration increaseDeclaration, dqContext ctx){
        dqVariable var = ctx.getVar(increaseDeclaration.getVarname());
        if (var == null){
            throw new RuntimeException("Value '" + increaseDeclaration.getVarname() + "' not found. Trace: " + ctx.trace());
        }
        else{
            if (var.getType() == d_DOUBLE){
                dqNumericBox nn = (dqNumericBox) var.getObject();
                var.setObject(new dqNumericBox(nn.getNumber().doubleValue() - 1, nn.getType()));
            }
            if (var.getType() == d_FLOAT){
                dqNumericBox nn = (dqNumericBox) var.getObject();
                var.setObject(new dqNumericBox(nn.getNumber().floatValue() - 1, nn.getType()));
            }
            if (var.getType() == d_INT){
                dqNumericBox nn = (dqNumericBox) var.getObject();
                var.setObject(new dqNumericBox(nn.getNumber().intValue() - 1, nn.getType()));
            }
            if (var.getType() == d_LONG){
                dqNumericBox nn = (dqNumericBox) var.getObject();
                var.setObject(new dqNumericBox(nn.getNumber().longValue() - 1, nn.getType()));
            }
        }
    }

    public dqType tt2dqType(Token.Type tt){
        if (tt== Token.Type.INTEGER){
            return d_INT;
        } else if (tt == Token.Type.DOUBLE) {
            return d_DOUBLE;
        } else if (tt == Token.Type.FLOAT) {
            return d_FLOAT;
        }
        else if (tt == Token.Type.LONG) {
            return d_LONG;
        }
        else{
            return null;
        }
    }

    public dqType inferNumType(dqObject a, dqObject b){
        if (!(a instanceof dqNumericBox a_n) || !(b instanceof dqNumericBox b_n)){
            throw new RuntimeException("Not numeric type");
        }

        dqType inferredType = null;

        if (a_n.getType().getType() == dqType.PrimitiveTypes.INT
                || b_n.getType().getType() == dqType.PrimitiveTypes.INT){
            inferredType = d_INT;
        }
         if (a_n.getType().getType() == dqType.PrimitiveTypes.LONG
                || b_n.getType().getType() == dqType.PrimitiveTypes.LONG){
            inferredType = d_LONG;
        }
         if (a_n.getType().getType() == dqType.PrimitiveTypes.FLOAT
                || b_n.getType().getType() == dqType.PrimitiveTypes.FLOAT){
            inferredType = d_FLOAT;
        }
        if (a_n.getType().getType() == dqType.PrimitiveTypes.DOUBLE
                 || b_n.getType().getType() == dqType.PrimitiveTypes.DOUBLE){
            inferredType = d_DOUBLE;
        }

        return inferredType;
    }

    public dqNumericBox fromInt(int num){
        return new dqNumericBox(num, d_INT);
    }

    public dqNumericBox fromFloat(float num){
        return new dqNumericBox(num, d_FLOAT);
    }

    public dqNumericBox fromDouble(double num){
        return new dqNumericBox(num, d_DOUBLE);
    }

    public dqNumericBox fromLong(long num){
        return new dqNumericBox(num, d_LONG);
    }

}
