package net.dq;

import net.dq.interpreter.Interpreter;
import net.dq.interpreter.dqNumericBox;
import net.dq.lexer.Lexer;
import net.dq.parser.Parser;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        String prog_1 = """
                fn float a_(float a){
                    return a;
                }
                
                fn float main(int c){
                    var a: float = -(c+5.2*2+5*(3-2));
                    a = 8.0*a_(a);
                    a_(10);
                    return a;
                }
                """;

        String prog_2 = """
                
                fn long main(int c){
                    var i: long = 5 as long + 3 as long;
                    return i;
                }
                """;

        String prog_3 = """
                
                fn bool main(){
                    var x: bool = (7 >= 7);
                    var y: long = (0) as long;
                    y++;
                    return (y) as bool;
                }
                """;

        String prog_4 = """
                fn int a(){
                    var i: long = 6;
                    
                    if (i ==6){
                    
                    } else if (i == 5){
                    
                    }
                    else {
                    
                    }
                    
                    
                    return (true) as int;
                }""";
        String factorial = """
                fn int factorial(int n){
                    if (n == 1){
                        return 1;
                    }
                    else {
                        return factorial(n-1)*n;
                    }
                }
                """;

        Lexer lx = new Lexer(factorial);
        lx.lex();
        Parser parser = new Parser();
        ArrayList<Parser.Node> nodes = parser.parse(lx.getTokens());

        Interpreter interpreter = new Interpreter(nodes);
        dqNumericBox obj = (dqNumericBox) interpreter.callFunction("factorial", interpreter.fromInt(14));
        System.out.println(obj.getNumber());
        System.out.println("A");
    }
}