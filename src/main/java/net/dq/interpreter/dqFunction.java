package net.dq.interpreter;

import net.dq.parser.Parser;

import java.util.ArrayList;

public class dqFunction extends dqObject {

    private final ArrayList<Parser.Node> functionBody;
    private final ArrayList<Parser.TypeIdentifierNode> args;
    private final Parser.TypeNode returnType;

    public dqFunction(ArrayList<Parser.Node> functionBody
            , ArrayList<Parser.TypeIdentifierNode> args,
                      Parser.TypeNode returnType){
        this.functionBody = functionBody;
        this.args = args;
        this.returnType = returnType;
    }

    public ArrayList<Parser.Node> getFunctionBody() {
        return functionBody;
    }

    public ArrayList<Parser.TypeIdentifierNode> getArgs() {
        return args;
    }

    public Parser.TypeNode getReturnType() {
        return returnType;
    }

    @Override
    public dqType getType() {
        return new dqType(dqType.PrimitiveTypes.FUNC);
    }
}
