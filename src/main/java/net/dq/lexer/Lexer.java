package net.dq.lexer;

import java.util.ArrayList;

public class Lexer {
    ArrayList<Token> tokens = new ArrayList<>();
    String t;
    int current_char;

    public Lexer(String text){
        t = text;
    }

    private int line = 1;

    public void lex() {
        char[] chars = t.toCharArray();
        current_char = 0;

        while (!(current_char > chars.length-1)){

            if (chars[current_char] == '\n'){
                current_char++;
                line++;
                continue;
            }
            else if (chars[current_char] == '\t' || chars[current_char] == ' '){
                current_char++;
                continue;
            }
            else if (chars[current_char] == ';'){
                tokens.add(new Token(Token.Type.COMMA_DOT).setLine(line));
            }
            else if (chars[current_char] == ','){
                tokens.add(new Token(Token.Type.COMMA).setLine(line));
            }
            else if (chars[current_char] == '.'){
                tokens.add(new Token(Token.Type.DOT).setLine(line));
            }
            else if (chars[current_char] == ':'){
                tokens.add(new Token(Token.Type.DOUBLE_DOT).setLine(line));
            }
            else if (chars[current_char] == '-'){
                tokens.add(new Token(Token.Type.MINUS).setLine(line));
            }
            else if (chars[current_char] == '+'){
                tokens.add(new Token(Token.Type.PLUS).setLine(line));
            }
            else if (chars[current_char] == '/'){
                tokens.add(new Token(Token.Type.DIVIDE).setLine(line));
            }
            else if (chars[current_char] == '*'){
                tokens.add(new Token(Token.Type.MULTIPLY).setLine(line));
            }
            else if (chars[current_char] == '='){
                if (current_char+1 < chars.length){
                    if (chars[current_char+1] == '='){
                        current_char++;
                        tokens.add(new Token(Token.Type.EQEQ).setLine(line));
                    }
                    else
                        tokens.add(new Token(Token.Type.EQUALS).setLine(line));
                }
                else
                    tokens.add(new Token(Token.Type.EQUALS).setLine(line));
            }
            else if (chars[current_char] == '!'){
                if (current_char+1 < chars.length){
                    if (chars[current_char+1] == '='){
                        current_char++;
                        tokens.add(new Token(Token.Type.NEQUALS).setLine(line));
                    }
                    else
                        tokens.add(new Token(Token.Type.EXCLAMATION_MARK).setLine(line));
                }
                else
                    tokens.add(new Token(Token.Type.EXCLAMATION_MARK).setLine(line));
            }
            else if (chars[current_char] == '<'){
                if (current_char+1 < chars.length){
                    if (chars[current_char+1] == '='){
                        current_char++;
                        tokens.add(new Token(Token.Type.ALBRACKET_EQ).setLine(line));
                    }
                    else
                        tokens.add(new Token(Token.Type.ALBRACKET).setLine(line));
                }
                else
                    tokens.add(new Token(Token.Type.ALBRACKET).setLine(line));
            }
            else if (chars[current_char] == '>'){
                if (current_char+1 < chars.length){
                    if (chars[current_char+1] == '='){
                        current_char++;
                        tokens.add(new Token(Token.Type.ARBRACKET_EQ).setLine(line));
                    }
                    else
                        tokens.add(new Token(Token.Type.ARBRACKET).setLine(line));
                }
                else
                    tokens.add(new Token(Token.Type.ARBRACKET).setLine(line));
            }
            else if (chars[current_char] == '&'){
                tokens.add(new Token(Token.Type.AND).setLine(line));
            }
            else if (chars[current_char] == '|'){
                tokens.add(new Token(Token.Type.OR).setLine(line));
            }
            else if (chars[current_char] == '?'){
                tokens.add(new Token(Token.Type.QUESTION_MARK).setLine(line));
            }
            else if (chars[current_char] == '('){
                tokens.add(new Token(Token.Type.LPAREN).setLine(line));
            }
            else if (chars[current_char] == ')'){
                tokens.add(new Token(Token.Type.RPAREN).setLine(line));
            }
            else if (chars[current_char] == '{'){
                tokens.add(new Token(Token.Type.LBRACE).setLine(line));
            }
            else if (chars[current_char] == '}'){
                tokens.add(new Token(Token.Type.RBRACE).setLine(line));
            }
            else if ("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM_".contains(String.valueOf(chars[current_char]))){
                tokens.add(constructIdentifier(chars).setLine(line));
                continue;
            }
            else if ("1234567890".contains(String.valueOf(chars[current_char]))){
                tokens.add(constructNumeral(chars).setLine(line));
                continue;
            }
            else {
                throw new RuntimeException("Unexpected character during lexing at line: " + line);
            }

            current_char++;
        }
    }

    public Token constructIdentifier(char[] chars){
        StringBuilder identifier = new StringBuilder();

        while (!(current_char > chars.length-1)){
            if (!"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890_".contains(String.valueOf(chars[current_char]))){
                break;
            }

            identifier.append(chars[current_char]);
            current_char++;
        }

        if (identifier.toString().equals("if") ||
                identifier.toString().equals("int") ||
                identifier.toString().equals("float") ||
                identifier.toString().equals("long") ||
                identifier.toString().equals("double") ||
                identifier.toString().equals("fn") ||
                identifier.toString().equals("var") ||
                identifier.toString().equals("return") ||
                identifier.toString().equals("as") ||
                identifier.toString().equals("else") ){
            return new Token(Token.Type.KEYWORD, identifier.toString());
        }

        return new Token(Token.Type.IDENTIFIER, identifier.toString());
    }


    public Token constructNumeral(char[] chars) {
        StringBuilder numeral = new StringBuilder();
        int dot_count = 0;
        int digit_count = 0;
        // long 19
        // integer 10
        // float 39
        // double 309
        int decimal_places_count = 0;
        // float 2
        // double 15

        while (true){
            if (current_char > chars.length-1){
                break;
            }

            if (!"1234567890.".contains(String.valueOf(chars[current_char]))){
                break;
            }

            if (chars[current_char] == '.'){
                dot_count += 1;
                if (dot_count > 1){
                    throw new RuntimeException("Too many dots at line " + line);
                }
            }
            else{
                if (dot_count == 1){
                    decimal_places_count++;
                }
                if (dot_count == 0){
                    digit_count++;
                }

            }
            // System.out.println(current_char);
            numeral.append(chars[current_char]);
            current_char++;
        }

        // System.out.println(numeral.toString());

        if (digit_count > 10 && decimal_places_count == 0 && digit_count <= 19){
            return new Token(Token.Type.LONG, Long.valueOf(numeral.toString()));
        }
        if (digit_count > 0 && decimal_places_count == 0){
            return new Token(Token.Type.INTEGER, Integer.valueOf(numeral.toString()));
        }
        if (digit_count > 0 && decimal_places_count < 3 && decimal_places_count > 0 && digit_count < 40){
            return new Token(Token.Type.FLOAT, Float.valueOf(numeral.toString()));
        }
        if (digit_count > 0 && decimal_places_count < 16 && decimal_places_count > 2 && digit_count < 310){
            return new Token(Token.Type.DOUBLE, Double.valueOf(numeral.toString()));
        }
        throw new RuntimeException("Illegal numeral at line " + line);
    }

    public ArrayList<Token> getTokens() {
        return tokens;
    }
}
