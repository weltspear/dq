package net.dq.lexer;

public class Token {

    private Object value;

    public enum Type{
        KEYWORD,
        IDENTIFIER,

        FLOAT,
        INTEGER,
        LONG,
        DOUBLE,

        COMMA,
        COMMA_DOT,
        DOUBLE_DOT,
        DOT,

        PLUS,
        MINUS,
        MULTIPLY,
        DIVIDE,

        EQUALS,

        EQEQ, // ==

        NEQUALS,

        ARBRACKET, // >
        ALBRACKET, // <

        ARBRACKET_EQ, // >=
        ALBRACKET_EQ, // <=

        AND,
        OR,

        EXCLAMATION_MARK,
        QUESTION_MARK,

        LPAREN,
        RPAREN,

        LBRACE,
        RBRACE,
    }

    private final Type type;

    public Token(Type t){
        type = t;
    }

    public Token(Type t, Object v){
        value = v;
        type = t;
    }

    public Type getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }
    private int line = 0;

    public Token setLine(int line){
        this.line = line;
        return this;
    }

    public int getLine() {
        return line;
    }
}
