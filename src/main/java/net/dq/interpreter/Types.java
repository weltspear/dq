package net.dq.interpreter;

public class Types {

    public final static dqType d_INT = new dqType(dqType.PrimitiveTypes.INT);
    public final static dqType d_FLOAT = new dqType(dqType.PrimitiveTypes.FLOAT);
    public final static dqType d_DOUBLE = new dqType(dqType.PrimitiveTypes.DOUBLE);
    public final static dqType d_LONG = new dqType(dqType.PrimitiveTypes.LONG);
    public final static dqType d_BOOL = new dqType(dqType.PrimitiveTypes.BOOL);
}
