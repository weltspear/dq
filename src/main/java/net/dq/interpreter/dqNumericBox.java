package net.dq.interpreter;

public class dqNumericBox extends dqObject{

    private Number number;
    private final dqType properType;

    public dqNumericBox(Number number, dqType properType){
        this.number = number;
        this.properType = properType;
    }

    public Number getNumber() {
        return number;
    }

    public dqType getType() {
        return properType;
    }

    public void setNumber(Number number) {
        this.number = number;
    }
}
